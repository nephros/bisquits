TEMPLATE = aux
INSTALLS += dbussvc dbusiface

dbussvc.files += dbus/services
dbussvc.path = /usr/share/dbus-1

dbusiface.files += dbus/interfaces
dbusiface.path += /usr/share/dbus-1
