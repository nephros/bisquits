<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="43"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="44"/>
        <source>What&apos;s %1?</source>
        <translation>Was ist %1?</translation>
    </message>
    <message>
        <source> </source>
        <translation type="vanished"> </translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="52"/>
        <source>%1 is ..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="54"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="55"/>
        <source>Copyright:</source>
        <translation>Urheberrecht:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="56"/>
        <source>License:</source>
        <translation>Lizenz:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="57"/>
        <source>Source Code:</source>
        <translation>Quellcode:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="58"/>
        <source>Credits</source>
        <translation>Danksagungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="60"/>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation>%1 Übersetzung</translation>
    </message>
</context>
<context>
    <name>DbusManager</name>
    <message>
        <location filename="../qml/components/DbusManager.qml" line="36"/>
        <location filename="../qml/components/DbusManager.qml" line="37"/>
        <location filename="../qml/components/DbusManager.qml" line="46"/>
        <location filename="../qml/components/DbusManager.qml" line="47"/>
        <source>Enable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/DbusManager.qml" line="55"/>
        <location filename="../qml/components/DbusManager.qml" line="56"/>
        <source>Disable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/DbusManager.qml" line="64"/>
        <source>Start %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListPage</name>
    <message>
        <location filename="../qml/pages/ListPage.qml" line="15"/>
        <source>Kegs o&apos; Bisquits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ListPage.qml" line="26"/>
        <source>Sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ListPage.qml" line="32"/>
        <source>offensive</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="41"/>
        <source>Ship&apos;s Bisquits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="42"/>
        <source>Today&apos;s Bisquit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="54"/>
        <source>Bags o&apos; Bisquits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="55"/>
        <source>Get a Bisquit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="52"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="53"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="36"/>
        <source>Settings</source>
        <comment>page title</comment>
        <translation type="unfinished">Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="50"/>
        <source>Notifier: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="51"/>
        <source>Status of the service responsible for publishing notifications.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="53"/>
        <location filename="../qml/pages/SettingsPage.qml" line="71"/>
        <source>ready to be enabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="55"/>
        <location filename="../qml/pages/SettingsPage.qml" line="73"/>
        <source>ready to be started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="57"/>
        <location filename="../qml/pages/SettingsPage.qml" line="75"/>
        <source>not ready.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="68"/>
        <source>Cookie Dispenser: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="69"/>
        <source>Status of the service responsible for generating a daily cookie.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="81"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="88"/>
        <source>Show Offensive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="89"/>
        <source>If enabled, you will receive bisquits that might offend you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="98"/>
        <source>If enabled, you will receive a new bisquit every now and then.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="116"/>
        <source>Publish a bisquit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="117"/>
        <source>A bisquit is dispensed in regular intervals.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="136"/>
        <source>Test Generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="140"/>
        <source>Test Notifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="40"/>
        <source>Service Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="97"/>
        <source>Show Notifications</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>harbour-bisquits</name>
    <message>
        <location filename="../qml/harbour-bisquits.qml" line="63"/>
        <source>No description available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>intervals</name>
    <message>
        <location filename="../qml/config/intervals.js" line="2"/>
        <source>Never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/config/intervals.js" line="3"/>
        <source>Hourly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/config/intervals.js" line="4"/>
        <source>Every Two Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/config/intervals.js" line="5"/>
        <source>Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/config/intervals.js" line="6"/>
        <source>Weekly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/config/intervals.js" line="7"/>
        <source>Monthly</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
