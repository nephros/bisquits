<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>AboutPage</name>
    <message>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation>Copyright:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Licens:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation>Källkod:</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Erkännanden</translation>
    </message>
    <message>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation>Översättningar: %1</translation>
    </message>
    <message>
        <source>What&apos;s %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is ..</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailsPanel</name>
    <message>
        <source>Unit Properties</source>
        <translation type="vanished">Enhetsegenskaper</translation>
    </message>
    <message>
        <source>Service Properties</source>
        <translation type="vanished">Tjänstegenskaper</translation>
    </message>
    <message>
        <source>Timer Properties</source>
        <translation type="vanished">Timeregenskaper</translation>
    </message>
    <message>
        <source>Mount Properties</source>
        <translation type="vanished">Monteringsegenskaper</translation>
    </message>
    <message>
        <source>Path Properties</source>
        <translation type="vanished">Sökvägsegenskaper</translation>
    </message>
    <message>
        <source>Unit name copied to clipboard</source>
        <translation type="vanished">Enhetsnamn kopierat till urklipp</translation>
    </message>
    <message>
        <source>Long press again to copy detailed info</source>
        <translation type="vanished">Långtryck igen för att kopiera detaljerad info</translation>
    </message>
    <message>
        <source>Unit details copied to clipboard</source>
        <translation type="vanished">Enhetsdetaljer kopierat till urklipp</translation>
    </message>
    <message>
        <source>Other Names</source>
        <translation type="vanished">Andra namn</translation>
    </message>
    <message>
        <source>Dependencies</source>
        <translation type="vanished">Beroenden</translation>
    </message>
    <message>
        <source>Unit File</source>
        <translation type="vanished">Enhetsfil</translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation type="vanished">Kopiera sökväg</translation>
    </message>
    <message>
        <source>Unit file path copied to clipboard</source>
        <translation type="vanished">Enhetsfilens sökväg kopierad till urklipp</translation>
    </message>
    <message>
        <source>Show Content</source>
        <translation type="vanished">Visa innehåll</translation>
    </message>
    <message>
        <source>%1 copied to clipboard</source>
        <translation type="vanished">%1 kopierad till urklipp</translation>
    </message>
    <message>
        <source>unit details</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="vanished">Enhetsdetaljer</translation>
    </message>
    <message>
        <source>unit name</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="vanished">Enhetsnamn</translation>
    </message>
    <message>
        <source>unit file path</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="vanished">Enhetsfilens sökväg</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <source>Ship&apos;s Bisquits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Today&apos;s Bisquit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sources</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>page title</comment>
        <translation>Inställningar</translation>
    </message>
    <message>
        <source>Application</source>
        <translation>Program</translation>
    </message>
    <message>
        <source>Show Notifications</source>
        <translation>Visa aviseringar</translation>
    </message>
    <message>
        <source>List Cleared.</source>
        <translation type="vanished">Listan rensad.</translation>
    </message>
    <message>
        <source>Sticky Notifications</source>
        <translation type="vanished">Fasta aviseringar</translation>
    </message>
    <message>
        <source>Advanced:</source>
        <translation type="vanished">Avancerat:</translation>
    </message>
    <message>
        <source>min</source>
        <translation type="vanished">min</translation>
    </message>
    <message>
        <source>Enable %1</source>
        <translation type="unfinished">Aktivera %1</translation>
    </message>
    <message>
        <source>Disable %1</source>
        <translation type="unfinished">Inaktivera %1</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="unfinished">Starta</translation>
    </message>
    <message>
        <source>Start %1</source>
        <translation type="unfinished">Starta %1</translation>
    </message>
    <message>
        <source>Service Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notifier: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Status of the service responsible for publishing notifications.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ready to be enabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ready to be started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>not ready.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cookie Dispenser: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Status of the service responsible for generating a daily cookie.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If enabled, you will receive a new bisquit every now and then.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Publish a bisquit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A bisquit is dispensed in regular intervals.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>weekly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hourly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>monthly</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnitItem</name>
    <message>
        <source>Ignore this session</source>
        <translation type="vanished">Undanta denna session</translation>
    </message>
    <message>
        <source>Ignore permanently</source>
        <translation type="vanished">Undanta permanent</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="vanished">Starta om</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="vanished">Aktivera</translation>
    </message>
    <message>
        <source>Disable</source>
        <translation type="vanished">Inaktivera</translation>
    </message>
    <message>
        <source>Mount</source>
        <translation type="vanished">Montera</translation>
    </message>
    <message>
        <source>Unmount</source>
        <translation type="vanished">Avmontera</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="vanished">Starta</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="vanished">Stoppa</translation>
    </message>
    <message>
        <source>No actions available.</source>
        <translation type="vanished">Inga åtgärder tillgängliga.</translation>
    </message>
    <message>
        <source>Remount</source>
        <translation type="vanished">Återmontera</translation>
    </message>
    <message>
        <source>next</source>
        <translation type="vanished">nästa</translation>
    </message>
    <message>
        <source>Enabling %1</source>
        <translation type="vanished">Aktiverar %1</translation>
    </message>
    <message>
        <source>Disabling %1</source>
        <translation type="vanished">Inaktiverar%1</translation>
    </message>
    <message>
        <source>Stopping %1</source>
        <translation type="vanished">Stoppar %1</translation>
    </message>
    <message>
        <source>Restarting %1</source>
        <translation type="vanished">Startar om %1</translation>
    </message>
</context>
<context>
    <name>UnitPage</name>
    <message>
        <source>About</source>
        <translation type="vanished">Om</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Inställningar</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Uppdatera</translation>
    </message>
    <message>
        <source>Switch to %1</source>
        <translation type="vanished">Växla till %1</translation>
    </message>
</context>
<context>
    <name>ViewUnitFile</name>
    <message>
        <source>Copy file path to Clipboard</source>
        <translation type="vanished">Kopiera filsökväg till urklipp</translation>
    </message>
    <message>
        <source>Copy contents to Clipboard</source>
        <translation type="vanished">Kopiera innehåll till urklipp</translation>
    </message>
    <message>
        <source>Open…</source>
        <translation type="vanished">Öppna…</translation>
    </message>
    <message>
        <source>Copy %1 to clipboard</source>
        <translation type="vanished">Kopiera %1 till urklipp</translation>
    </message>
    <message>
        <source>file path</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="vanished">filsökväg</translation>
    </message>
    <message>
        <source>%1 copied to clipboard</source>
        <translation type="vanished">%1 kopierad till urklipp</translation>
    </message>
    <message>
        <source>file contents</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="vanished">innehåll</translation>
    </message>
    <message>
        <source>Share %1…</source>
        <translation type="vanished">Dela %1…</translation>
    </message>
</context>
<context>
    <name>harbour-sailord</name>
    <message>
        <source>Enabling %1</source>
        <translation type="vanished">Aktiverar %1</translation>
    </message>
    <message>
        <source>Disabling %1</source>
        <translation type="vanished">Inaktiverar%1</translation>
    </message>
    <message>
        <source>Restarting %1</source>
        <translation type="vanished">Startar om %1</translation>
    </message>
    <message>
        <source>Stopping %1</source>
        <translation type="vanished">Stoppar %1</translation>
    </message>
    <message>
        <source>%1 successful.</source>
        <comment>arg is an operation, such as &apos;restarting service X&apos;</comment>
        <translation type="vanished">%1 slutfört.</translation>
    </message>
    <message>
        <source>Success:</source>
        <translation type="vanished">Slutfört:</translation>
    </message>
    <message>
        <source>%1 failed.</source>
        <comment>arg is an operation, such as &apos;restarting service X&apos;</comment>
        <translation type="vanished">%1 misslyckades.</translation>
    </message>
    <message>
        <source>Failure:</source>
        <translation type="vanished">Misslyckat:</translation>
    </message>
    <message>
        <source>Preset %1</source>
        <translation type="vanished">Förinställ %1</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="obsolete">Aktivera</translation>
    </message>
    <message>
        <source>Enable %1</source>
        <translation type="vanished">Aktivera %1</translation>
    </message>
    <message>
        <source>Disable</source>
        <translation type="obsolete">Inaktivera</translation>
    </message>
    <message>
        <source>Disable %1</source>
        <translation type="vanished">Inaktivera %1</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="vanished">Starta</translation>
    </message>
    <message>
        <source>Start %1</source>
        <translation type="vanished">Starta %1</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="vanished">Stoppa</translation>
    </message>
    <message>
        <source>Stop %1</source>
        <translation type="vanished">Stoppa %1</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="vanished">Starta om</translation>
    </message>
    <message>
        <source>Restart %1</source>
        <translation type="vanished">Starta om %1</translation>
    </message>
</context>
</TS>
