import QtQuick 2.6
import Sailfish.Silica 1.0

CoverBackground {
    id: coverPage

    /*
    Image {
        source: "./background.png"
        z: -1
        anchors {
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
        }
        sourceSize.width: parent.width
        fillMode: Image.PreserveAspectFit
        opacity: 0.2
    }
    */

    Label { id: bisquit
        anchors.centerIn: parent
        height: Math.max(implicitHeight,Theme.itemsizeLarge)
        //width: parent.width
        //wrapMode: Text.Wrap
        font.pixelSize: Theme.fontSizeTiny
        text: '<pre>' + fortune + '</pre>'
        rotation: 90
    }
    CoverActionList {
        CoverAction { iconSource: "image://theme/icon-m-sync";
            onTriggered: {fiface.get(offensive)}
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
