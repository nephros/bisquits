/*

Apache License 2.0

Copyright (c) 2022 Peter G. (nephros)

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License.  
You may obtain a copy of the
License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import "config/sources.js" as Sources
import "config/cookies.js" as Cookies
import "config/intervals.js" as Repeat
import "pages"
import "cover"
import "components"

ApplicationWindow {
    id: app

    property alias fortune: fortunes.currentCookie

    DbusFortune { id: fiface
        onCookieChanged: app.fortune = cookie
    }

    ListModel{ id: intervals
        function populate(s) {
            for (var i=0; i<s.length; i++) {
               append(s[i]);
            }
        }
    }

    ListModel{ id: sources
        function populate(s) {
            for (var i=0; i<s.length; i++) {
               append(s[i]);
            }
        }
    }

    ListModel{ id: cookies
        function populate(s) {
            for (var i=0; i<s.length; i++) {
                var o = s[i];
                const d = Cookies.descriptions
                const v = (d[o["id"]]); // cookie id from "cookies" is key in "descriptions"
                o["desc"] = v ? v : qsTr("No description available");
               append(o);
            }
        }
    }
    Component.onCompleted: {
        // for sailjail
        Qt.application.domain  = "sailfish.nephros.org";
        Qt.application.version = "unreleased";
        console.info("Intialized", Qt.application.name, "version", Qt.application.version, "by", Qt.application.organization );
        console.debug("Parameters: " + Qt.application.arguments.join(" "))
        intervals.populate(Repeat.intervals);
        sources.populate(Sources.fortunes);
        cookies.populate(Cookies.boring);
        cookies.populate(Cookies.offensive);
    }

    // application settings:
    property alias interval: config.interval
    property alias fortunes: fortunes.enabled
    property alias offensive: fortunes.offensive
    ConfigurationGroup  { id: settings
        path: "/org/nephros/" + Qt.application.name
    }
    ConfigurationGroup { id: config
        scope: settings
        path:  "app"
        property bool notify: true
        property int interval: 0
    }
    ConfigurationGroup { id: fortunes
        scope: settings
        path:  "app"
        property var enabled: []
        property bool offensive: false
        property string currentCookie: "none yet"
    }

    initialPage: Component { MainPage{} }
    cover: CoverPage{}

}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
