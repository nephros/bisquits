import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Dialog {
    id: page

    property var selected: { return app.fortunes }
    onAccepted: { app.fortunes = selected }

    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: col.height - head.height
        DialogHeader { id: head ; title: qsTr("Kegs o' Bisquits") }
        Column {
            id: col
            width: parent.width - Theme.horizontalPageMargin
            anchors.top: head.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: Theme.paddingLarge
            bottomPadding: Theme.itemSizeLarge
            add:      Transition { FadeAnimation { duration: 1200 } }
            move:     Transition { FadeAnimation { duration: 1200 } }
            populate: Transition { FadeAnimation { duration: 1200 } }
            SectionHeader { text: qsTr("Sources") }
            Repeater {
                model: cookies
                delegate: TextSwitch {
                    checked: !offensive && !disable
                    description: desc
                    text: id[0].toUpperCase() + id.substr(1,id.length) + ( offensive ? " (" + qsTr("offensive") + ")" : "" )
                    onCheckedChanged: { page.selected.push(id) }
                }
            }
        }
        VerticalScrollDecorator {}
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
