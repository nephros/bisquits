import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.DBus 2.0
import "../components"

Page {
    id: settingsPage

    //onStatusChanged: (status === PageStatus.Deactivating) ? app.refresh() : 0

    readonly property string notifierName:  "org_2enephros_2esailfish_2enotifier_2epath"
    readonly property string generatorName: "org_2enephros_2esailfish_2eFortune_2eservice"

    DbusUnit {
        id: notifier
        unitName: notifierName
    }
    DbusUnit {
        id: generator
        unitName: generatorName
    }

    DbusManager {
        id: manager
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: col.height
        Column {
            id: col
            spacing: Theme.paddingSmall
            bottomPadding: Theme.itemSizeLarge
            width: parent.width - Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
            PageHeader{ title: qsTr("Settings", "page title")}
            SectionHeader {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Service Status")
            }
            TextSwitch{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                highlighted: false
                checked: (notifier.loadState == "loaded") && (notifier.activeState == "active")
                busy: ((notifier.activeState == "activating") || (notifier.activeState == "deactivating"))
                automaticCheck: false
                //text: qsTr("Notifier Status: %1").arg(svcstatus)
                text: qsTr("Notifier: %1").arg(notifier.loadState + "/" + notifier.activeState)
                description: qsTr("Status of the service responsible for publishing notifications.")
                property string svcstatus: if ( (notifier.loadState == "loaded") && (notifier.activeState == "active") ) {
                        return qsTr("ready to be enabled.")
                    } else if ( (notifier.loadState == "loaded") && (notifier.activeState == "inactive") ) {
                        return qsTr("ready to be started.")
                    } else {
                        return qsTr("not ready.")
                    }
            }
            TextSwitch {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                highlighted: false
                checked: (generator.loadState == "loaded") && (generator.activeState == "active")
                busy: ((generator.activeState == "activating") || (generator.activeState == "deactivating"))
                automaticCheck: false
                //text: qsTr("Cookie Dispenser Status: %1").arg(svcstatus)
                text: qsTr("Cookie Dispenser: %1").arg(generator.loadState + "/" + generator.activeState)
                description: qsTr("Status of the service responsible for generating a daily cookie.")
                property string svcstatus: if ( (generator.loadState == "loaded") && (generator.activeState == "active") ) {
                        return qsTr("ready to be enabled.")
                    } else if ( (generator.loadState == "loaded") && (generator.activeState == "inactive") ) {
                        return qsTr("ready to be started.")
                    } else {
                        return qsTr("not ready.")
                    }
            }
            SectionHeader {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Application")
            }
            TextSwitch{ id: offsw // ;)
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: offensive
                automaticCheck: true
                text: qsTr("Show Offensive")
                description: qsTr("If enabled, you will receive bisquits that might offend you.")
            }
            TextSwitch{ id: notifysw
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: generator.activeState == "active"
                busy: ((generator.activeState == "activating") || (generator.activeState == "deactivating"))
                automaticCheck: true
                text: qsTr("Show Notifications")
                description: qsTr("If enabled, you will receive a new bisquit every now and then.")
                onClicked: {
                    //if (checked) {
                    //    manager.enable(notifierName)
                    //    manager.enable(generatorName)
                    //    notifier.start()
                    //    generator.start()
                    //} else {
                    //    generator.stop()
                    //    generator.disable()
                    //}
                }
            }
            ComboBox {
                id: intervalCBox
                enabled: notifysw.checked
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                label: qsTr("Publish a bisquit");
                description: qsTr("A bisquit is dispensed in regular intervals.");
                currentIndex: app.interval ? app.interval : 0
                menu: ContextMenu { id: ctx
                    Repeater {
                        model: intervals
                        delegate: MenuItem { text: model.text; property string name: model.name }
                    }
                }
                onCurrentIndexChanged: {
                    if (( currentIndex != app.interval ) && ( currentIndex > -1 )) {
                        app.interval = currentIndex
                        manager.createTimer(currentIndex)
                    }

                }
            }
            ButtonLayout {
                anchors.horizontalCenter: parent.horizontalCenter
            Button {
                text: qsTr("Test Generator")
                onClicked: generator.start();
            }
            Button {
                text: qsTr("Test Notifier")
                onClicked: notifier.start();
            }
            }
        }
        VerticalScrollDecorator {}
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
