var intervals = [
    { name: "never",   text: qsTr("Never"),            systemdWhen: "" },
    { name: "hourly",  text: qsTr("Hourly"),           systemdWhen: "hourly" },
    { name: "2hours",  text: qsTr("Every Two Hours"),  systemdWhen: "*-*-* 07..22/2:00:00" },
    { name: "daily",   text: qsTr("Daily"),            systemdWhen: "*-*-* 08:00:00" },
    { name: "weekly",  text: qsTr("Weekly"),           systemdWhen: "Mon 08:00:00" },
    { name: "monthly", text: qsTr("Monthly"),          systemdWhen: "*-*-01 08:00:00" },
];

