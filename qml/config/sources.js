var	fortunes = [
		{ "id": "simpsons", "url": "",
			"displayName": "The Simpsons", "url": "",
			"description": "",
			"readme": ""
		},
		{ "id": "kernel", "url": "",
			"displayName": "Linux Kernel",
			"description": "Quotes from the Linux kernel source code",
			"readme": ""
		},
		{ "id": "freebsd-classic", "url": "https://github.com/HubTou/fortunes-freebsd-classic",
			"displayName": "Classical FreeBSD",
			"description": "classical data files for the FreeBSD fortune utility",
			"readme": "https://raw.githubusercontent.com/HubTou/fortunes-freebsd-classic/main/README.md"
		},
		{ "id": "historical", "url": "https://github.com/HubTou/fortunes-freebsd-classic",
			"displayName": "Historical",
			"description": "data files for the fortune utilities published in historical versions of Research & BSD Unix, between January 1979 and June 1993",
			"readme": "https://raw.githubusercontent.com/HubTou/fortunes-historical/main/README.md"
		}
	];
