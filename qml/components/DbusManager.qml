import QtQuick 2.6
import Nemo.DBus 2.0
//import "../config/intervals.js" as Repeat


DBusInterface {
    id: manager
    bus: DBus.SessionBus
    service: "org.freedesktop.systemd1"
    path: "/org/freedesktop/systemd1"
    iface: "org.freedesktop.systemd1.Manager"

    function createTimer(when) {
        see: https://github.com/sailfishos/nemo-qml-plugin-dbus/commit/62e4055565ed5c558d30607938cc5f4abeebcec3
        console.warn("createTimer: Not implemented, need type = a{sv} support")
        return;
        console.debug("Should make a Timer as:", when)
        var timerName = "org.nephros.sailfish.BisquitsNotifier.timer"
        var systemdWhen = intervals.get(when).systemdWhen
        typedCall('StartTransientUnit', [
            { "type": "s", "value": timerName },
            { "type": "s", "value": "replace" },
            { "type": "a{sv}", "value": [
                [
                    { name: "Description", value: "Bisquit Ration Timer"},
                    { name: "OnCalendar",  value: systemdWhen}
                ],
                [
                    { name: "Description", value: "Bisquit Ration Service"},
                    { name: "ExecStart", value: "/bin/true"},
                    { name: "Type", value: "oneshot"},
                ]
            ] },
            undefined
        ],
            function(result) { successMsg(qsTr("Enable %1").arg(u), result[1]); },
            function(result) { failMsg(qsTr("Enable %1").arg(u), result[1]) }
        );
    }
    function enable(u) {
        typedCall('EnableUnitFiles', [
            { "type": "as", "value": [u] },
            { "type": "b", "value": false },
            { "type": "b", "value": false },
        ],
            function(result) { successMsg(qsTr("Enable %1").arg(u), result[1]); app.refresh() },
            function(result) { failMsg(qsTr("Enable %1").arg(u), result[1]) }
        );
    }
    function disable(u) {
        typedCall('DisableUnitFiles', [
            { "type": "as", "value": [u] },
            { "type": "b", "value": false },
        ],
            function(result) { successMsg(qsTr("Disable %1").arg(u), result); app.refresh() },
            function(result) { failMsg(qsTr("Disable %1").arg(u), result) }
        );
    }
    function start(u) {
        //console.debug("dbus start unit", u );
        call('StartUnit',
            [u, "replace"],
            function(result) { console.debug("Job:", JSON.stringify(result)); },
            function(result) { failMsg(qsTr("Start %1").arg(u), result) }
        );
    }
}
// vim: ft=javascript expandtab ts=4 sw=4 st=4

