import QtQuick 2.6
import Nemo.DBus 2.0

DBusInterface {
    property string unitName
    bus: DBus.SessionBus
    service: "org.freedesktop.systemd1"
    path: "/org/freedesktop/systemd1/unit/" + unitName
    iface: "org.freedesktop.systemd1.Unit"
    propertiesEnabled: true

    property string activeState
    property string loadState
    function start() {
        call('Start',
            ["replace",],
            function(result) { console.debug("Job:", JSON.stringify(result)); },
            function(result) { console.debug("Failed:", JSON.stringify(result)); }
        );
    }
    function stop() {
        call('Stop',
            ["replace",],
            function(result) { console.debug("Job:", JSON.stringify(result)); },
            function(result) { console.debug("Failed:", JSON.stringify(result)); }
        );
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4

