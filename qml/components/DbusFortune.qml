import QtQuick 2.6
import Nemo.DBus 2.0

DBusInterface {
    bus: DBus.SessionBus
    service: "org.nephros.sailfish.Fortune"
    path: "/org/nephros/sailfish/Fortune"
    iface: "org.nephros.sailfish.Fortune"

    property string cookie
    property string list

    function handleResult(result){
        cookie = result
    }
    function get(offensive) {
        call("getFortune", [ offensive ],
            function(result) { console.debug("OK:", JSON.stringify(result)); handleResult(result) },
            function(result) { console.warn("Failed:", JSON.stringify(result)); }
        );
    }
    function getLong(offensive) {
        call("getLongFortune", [ offensive ],
            function(result) { console.debug("OK:", JSON.stringify(result)); handleResult(result) },
            function(result) { console.warn("Failed:", JSON.stringify(result)); }
        );
    }
    function getList() {
        call("getFileList", undefined,
            function(result) { console.debug("OK:", JSON.stringify(result)); handleResult(result) },
            function(result) { console.warn("Failed:", JSON.stringify(result)); }
        );
    }
    function getWithArgs(args) {
        call("getFortuneWithArgs", [ args ],
            function(result) { console.debug("OK:", JSON.stringify(result)); handleResult(result) },
            function(result) { console.warn("Failed:", JSON.stringify(result)); }
        );
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4

