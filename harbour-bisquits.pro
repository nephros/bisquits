
TARGET = harbour-bisquits
CONFIG += lrelease embed_translations sailfishapp_i18n

lupdate_only {
SOURCES += \
    qml/$${TARGET}.qml \
    qml/pages/*.qml \
    qml/cover/*.qml \
    qml/config/*.js \
    qml/components/*.qml

}

TRANSLATIONS += translations/$${TARGET}-en.ts \
                translations/$${TARGET}-de.ts 

desktop.files = $${TARGET}.desktop
desktop.path = /usr/share/applications
INSTALLS += desktop

qml.files = qml
qml.path = /usr/share/$${TARGET}

INSTALLS += qml

OTHER_FILES += $$files(rpm/*)

include(translations/translations.pri)
include(systemd/systemd.pri)
include(dbus/dbus.pri)
include(icons/icons.pri)
