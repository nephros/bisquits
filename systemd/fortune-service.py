#!/usr/bin/env python3

usage = '''Usage:
python fortune-service.py &
python fortune-client.py
python fortune-async-client.py
python fortune-client.py --exit-service
'''

# Copyright (C) 2004-2006 Red Hat Inc. <http://www.redhat.com/>
# Copyright (C) 2005-2007 Collabora Ltd. <http://www.collabora.co.uk/>
# Copyright (C) 2023 Peter G. <sailfish@nephros.org>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the 'Software'), to deal in the Software without
# restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

from gi.repository import GLib


import dbus
import dbus.service
import dbus.mainloop.glib

#import sys
import os
import subprocess
#import traceback

import re

def get_fortune(args):
    cmd = str('/usr/bin/fortune' + ' -' + args).split()
    print('Calling', cmd)
    #f = subprocess.run(cmd, stdout=subprocess.PIPE)
    f = subprocess.run(cmd, capture_output=True)
    return f.stdout

class Fortune(dbus.service.Object):

    @dbus.service.method('org.nephros.sailfish.Fortune',
                         in_signature='b', out_signature='s')
    def getFortune(self, offensive):
        args = 's'
        if offensive:
            args += 'o'
        return get_fortune(args)

    @dbus.service.method('org.nephros.sailfish.Fortune',
                         in_signature='b', out_signature='s')
    def getLongFortune(self, offensive):
        args = 'l'
        if offensive:
            args += 'o'
        return get_fortune(args)
    @dbus.service.method('org.nephros.sailfish.Fortune',
                         in_signature='s',
                         out_signature='s')
    def getFortuneWithArgs(self, args):
        # user input: guard against redirections at least:
        #return get_fortune(args.replace(';','').replace('|','').replace('<','').replace('>','').replace('&',''))
        return get_fortune(re.sub('[\n;|><]+.*$', '', args))
    @dbus.service.method('org.nephros.sailfish.Fortune',
                         in_signature='',
                         out_signature='as')
    def getFileList(self):
        lst = get_fortune('f')
        arr = lst.splitlines()
        print("arr is:", arr)
        res = []
        for e in arr:
            print("adding:", e)
            res.append(e.strip())
        print("returning:", res)
        return res

    def Exit(self):
        mainloop.quit()


if __name__ == '__main__':
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    session_bus = dbus.SessionBus()
    name = dbus.service.BusName('org.nephros.sailfish.Fortune', session_bus)
    object = Fortune(session_bus, '/org/nephros/sailfish/Fortune')

    mainloop = GLib.MainLoop()
    print('Running fortune service.')
    print(usage)
    mainloop.run()
