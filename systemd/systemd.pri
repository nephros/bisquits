TEMPLATE = aux
INSTALLS += units script

units.files += \
	 systemd/$${TARGET}.service \
#	 systemd/$${TARGET}.timer

units.path = /usr/lib/systemd/user

script.files += systemd/fortune-service.py
script.path = /usr/libexec

